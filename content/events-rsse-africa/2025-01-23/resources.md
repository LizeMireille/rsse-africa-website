---
widget: blank
headless: true  # This file represents a page section.
title: "Meetup Resources"
subtitle: 

weight: 30   

design:
    columns: "1"
---

<br>

<div class="container">
    <div class="row">
        <div class="col-6">
            <p align="center">
                View our <a href="https://youtu.be/6GJZ8mO4t9Q?si=ihzhs4w7r2-neD7S" target="_blank">Recording</a>
                <br>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/6GJZ8mO4t9Q?si=LwJfGnXagpB8DM3S" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
            </p>
        </div>
        <div class="col-3">
            <p align="center">
                Access our <a href="https://doi.org/10.5281/zenodo.14833913" target="_blank">Resources Sheet</a>
                <br>
                <a href="https://doi.org/10.5281/zenodo.14833913" target="_blank"><img src="resource-sheet.png" width="70%"></a>
            </p>
        </div>
        <div class="col-3">
            <p align="center">
                Read our <a href="../../post/2025/open-science-episode4/">summary blog post</a>
                <br>
                <a href="../../post/2025/open-science-episode4/"><img src="blog-post.png" width="70%"></a>
            </p>
        </div>
    </div>
</div>