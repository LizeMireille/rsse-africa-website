---
# Homepage
type: widget_page

title: A Conversation with Researchers Who Code

# Homepage is headless, other widget pages are not.
headless: false
---
