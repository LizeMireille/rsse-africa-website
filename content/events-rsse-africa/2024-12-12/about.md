---
widget: blank
headless: true  # This file represents a page section.
title: "Episode 3: Opening Up Research Code"
subtitle: |
    <br>

    <h3>12 December 2024 @ 8:30 - 10:00 am UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?iso=20241212T0830&p1=1440&ah=1&am=30">(your local time)</a></h3>

    <br>

    Join RSSE Africa and the RSE Asia Association as we showcase the work of researchers who code, learn about good practices for writing research code, and connect with the global research software community!

    In the third meetup of the series on "Enabling Open Science through Research Code", we will meet several researchers from Africa and Asia who spend a significant part of their day programming. In this session, we will discuss ways to make your research code more open and usable to your future self and others. The tips and tricks our speakers will share could also help get recognition for the code you develop through traditional mechanisms such as publications.

    Some topics that will be covered include:

      An overview of motivations, benefits, and risks of sharing and reusing code
    
      Identify whether you can use other people's software
    
      What contributes to the reusability of code
    
      Open source licencing - when, how, what
    
      GitHub and more
    
      Making software citable including publishing papers on software.

design:
    columns: "1"
---


{{% callout info %}}
  A useful open educational resource related to these topics are available from [CodeRefinery](https://coderefinery.org/).

  __[Follow the CodeRefinery lesson on Social Coding and Open Software](https://coderefinery.github.io/social-coding/)__

{{% /callout %}}
