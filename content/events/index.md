---
title: Events
type: landing

sections:

  - block: markdown
    id: toc
    content:
      title: Events
      subtitle: RSSE Africa runs community meetups but also share opportunities offered through communities and organisations in our network. 
      text: |

        <div class="row">
          <div class="col-md-6">
            <p align="center">
              <a href="../community_meetups/"><img src="../static-images/africa.svg" ></a>
            </p>
            <p align="center"> <strong><a href="../community_meetups/">RSSE Africa Events </a></strong></p>
          </div>
          
          <div class="col-md-6">
            <p align="center">
              <a href="#globe"><img src="../static-images/globe.svg" ></a>
            </p>
            <p align="center"> <strong><a href="#globe">Global Events </a></strong></p>
          
          </div>
        </div>
     
  - block: markdown
    id: globe
    content:
      title: "Events from Around the Globe"
      image:
        filename: 
      text: |               
      
        This page lists events organised by the RSSE Africa community and others that may be relevant to people based in Africa interested in research software and infrastructure.

        <script src="https://apps.elfsight.com/p/platform.js" defer></script>
        <div class="elfsight-app-b5ce2a01-998a-4e13-869c-5ed355d891ab"></div>



---


