---
title: An update on astronomical activities in Africa
subtitle: 

# Summary for listings and search engines
summary: This week the International Astronomical Union (IAU) General Assembly - the largest astronomical meetup in the world, will kick off in South Africa. It’s the first time the community will unite under the African skies since the IAU was founded in 1919 - more than 100 years ago!


# Link this post with a project
projects: []

# Date published
date: '2024-08-07T00:00:00Z'

# Date updated
lastmod: '2024-08-07T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Obelixlatino via Pixabay'
  focal_point: ''
  placement: 
  preview_only: true



authors:
  - Mireille Grobbelaar
  - Anelda van der Walt
  - Richard Dushime
  

tags:
  - Astronomy
  - IAU General Assembly
  - SKA Observatory

categories:
  - Astronomy
  - IAU General Assembly
  - SKA Observatory
---

Astronomy on the African continent has been growing rapidly, making substantial contributions to global scientific knowledge while fostering regional development. Key projects driving this progress include the [Square Kilometre Array Observatory](https://www.skao.int/en) (SKAO), [Development in Africa with Radio Astronomy](https://www.dara-project.org/) (DARA), and the [African Very-long-baseline interferometry Network](https://www.sarao.ac.za/science/avn/) (AVN). 


<a title="Square Kilometre Array Organisation (SKAO), CC0, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Square_Kilometre_Array_Organisation_members.svg"><img width="512" alt="Square Kilometre Array Organisation members" src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Square_Kilometre_Array_Organisation_members.svg/512px-Square_Kilometre_Array_Organisation_members.svg.png?20230825071354"></a>*The Square Kilometre Array Organisation members.*

<br>


The SKAO aims to ‘transform our understanding of the universe, and deliver benefits to society through global collaboration and innovation.’ Operating on three continents, SKAO has South Africa as its African hub, hosting the Science Operations Centre (SOC) in Cape Town and the Engineering Operations Centre (EOC) near Carnarvon in the Northern Cape. When the project was expanded, the Northern Cape telescope was renamed “MeerKAT”—a play on “more of KAT” and a nod to the meerkat, a beloved small mammal native to the Karoo region. 

{{< cta cta_text="Learn more about SKAO" cta_link="https://www.skao.int/en" cta_new_tab="true" >}}

<a title="Image by Wolfgang Weiser from Pixabay" href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=8345747"><img width="512" alt="The well-loved meerkat" src="https://cdn.pixabay.com/photo/2023/10/27/16/29/meerkat-8345747_1280.jpg"></a>*The well-loved meerkat can often be seen hanging around in a sunny spot with the family - always on the lookout for an unexpected predator.*

<br>



On the other hand, DARA is a joint UK-South African initiative that seeks to develop high-tech skills using radio astronomy in several African countries, including Ghana, Kenya, Botswana, Namibia, Zambia, Madagascar, Mozambique, and Mauritius. This initiative highlights the importance of astronomy in driving economic growth and developing a skilled workforce across the continent. The project’s goal is to train young people in advanced STEM skills, leading to the creation of self-sustaining space-sector hubs in their home countries.


{{< cta cta_text="Learn more about DARA" cta_link="https://www.dara-project.org/" cta_new_tab="true" >}}

The AVN is a system of very-long-baseline interferometry (VLBI) capable radio telescopes spread across Africa. Its primary aim is to develop the necessary skills, regulatory frameworks, and institutional capacities in SKA partner countries. This effort is crucial for maximizing African involvement in the SKA and facilitating engagement in the development and scientific use of SKA pathfinder technologies. 

{{< cta cta_text="Learn more about AVN" cta_link="https://www.sarao.ac.za/science/avn/" cta_new_tab="true" >}}

**RSEs and astronomy in Africa**

The growth in astronomy and other scientific fields on the continent is also enhancing the role of RSSEs (Research Software/Systems Engineers). RSSEs are essential in developing, maintaining, and optimizing the software tools that researchers rely on. At SKAO, software development is a critical aspect of operations, employing numerous individuals in programming and development roles. The SKAO’s [developer portal](https://developer.skao.int/en/latest/getting-started/onboarding.html#information-about-the-ska) further highlights the significant role of software in astronomical research and showcases the contributions of RSEs to the field.

Similarly, [Atlar Innovation](https://www.skao.int/en/impact/458/software-firm-born-skao-construction-opportunities) is a prime example of how RSEs contribute to and benefit from astronomical projects. This Portuguese software company started as an SKA spin-off and has since evolved into a specialist firm in SKA software development. Atlar hosts several postgraduate placements, providing students with valuable industry experience and opportunities offered by major international projects like the SKAO.

<br>

**The IAU Assembly comes to Cape Town**

<iframe width="560" height="315" src="https://www.youtube.com/embed/MzR_dthayRE?si=A7wClHBQ94VsG2ei" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<br>


The conversation surrounding astronomy in Africa will continue to thrive at the upcoming [International Astronomical Union (IAU) General Assembly 2024](http://astronomy2024.org), hosted in Cape Town, South Africa, from August 6th to 15th. This will be the 32nd IAU Assembly and is the first time it is coming to Africa. It will showcase the latest in astronomical research and highlight the growing role of African countries in the global scientific arena. The event is expected to draw astronomers, RSEs, and scientists from around the world, providing a platform for collaboration and knowledge exchange. The assembly will cover a range of topics, from cutting-edge research findings to discussions on integrating indigenous knowledge systems into modern astronomy.

*Scientific sessions, including poster sessions can be attended in person or virtually. There’s still time to register! It is also possible to [donate a registration](https://astronomy2024.org/registration/donate-your-registration/).*

Through the synergy of projects like SKAO, DARA, and AVN, and events like the IAU General Assembly, Africa is solidifying its place in the global astronomical community. These efforts not only advance scientific understanding but also drive economic development and capacity building, showcasing the integral role of RSEs in the process.Follow the latest updates from the General Assembly via [social media](https://x.com/astronomy2024) to stay informed and be part of this remarkable journey!


Stay updated on these and other exciting RSSE developments in Africa by  signing up for the [RSSE Africa newsletter](https://africa.us14.list-manage.com/subscribe?u=35d5db26d3b108b9ef9b9ac43&id=55e9f5a692).
