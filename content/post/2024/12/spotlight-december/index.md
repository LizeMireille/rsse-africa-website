---
title: "🇿🇦 December 2024: Meet Cael Marquard"

subtitle: 

# Summary for listings and search engines
summary:  Welcome 👋 to our December 2024 community member spotlight! In this edition we're introducing Cael Marquard from South Africa.

# Link this post with a project
projects: []

# Date published
date: '2024-12-12T11:00:00Z'


# Date updated
lastmod: '2024-12-12T11:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Cael Marquard'
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Cael Marquard

tags:
  - Community members
  - Computer Science
  - Machine Learning
  - South Africa

categories:
  - Spotlights
  - South Africa
---

Our December spotlight shines on Cael Marquard from South Africa. Cael is a Computer Science postgraduate student at the University of Cape Town and a part-time software developer at SADiLaR.

## In short
---

- __Preferred name and surname__: Cael Marquard
- __Affiliation__ (where do you work or study): Studying at University of Cape Town, contracting at SADiLaR
- __Role__:   MSc by dissertation (soon to start) at UCT, software development at SADiLaR
- __LinkedIn__: https://www.linkedin.com/in/cael-marquard/
- __ORCID__: https://orcid.org/0009-0003-9420-6199
- __Email__: cael.marquard@gmail.com

---


{{< spoiler text="__:musical_score: What is your favourite song?__" >}}

:sound: [Life Esidimeni](https://www.youtube.com/watch?v=enULchNtTt4&t=1397s) – _Malcolm Jiyane_ 

{{< /spoiler >}}

{{< spoiler text="__:books: Can you describe your background and your current role?__" >}}

#### What did you study?

 I studied a BSc in Computer Science and Xhosa Communication at UCT, and I should graduate soon from my Honours in Computer Science at UCT too. After that, I will be starting a Masters by dissertation in Computer Science also at UCT.

#### What is the title of your current role(s)?

I’m soon to be an MSc student in Computer Science at UCT, and I am a developer (independent contractor) for SADiLaR.

#### Give a one-sentence summary of what you do in these roles

At SADiLar, I work on software for terminology management and higher education support, and in my studies at UCT I research machine learning for African languages.

{{< /spoiler >}}

{{< spoiler text="__:briefcase: Describe a typical workday... Expand specifically on the amount of time you spend coding__" >}}

#### How much time do you spend coding?

When doing my part-time work for SADiLaR, probably 90%, and when working on my studies, maybe 50-60%.

#### How many projects do you work on?

Two for SADiLaR, likely one for my MSc, and another one (IsiXhosa.click) in addition.

#### Give some keywords that summarise the topics of your projects.

Lexicography, OER, linguistics, natural language processing, terminology management, web development

#### How often do you work with non-coding researchers?

I don’t think I have before!

{{< /spoiler >}}

{{< spoiler text="__:school: Tell us more about the organisation where you work?__" >}}

#### What is their primary objective?

I’m new and part-time at SADiLaR, but as I understand, it is to support research in languages and more broadly digital humanities. At UCT, the focus of the Computer Science Department is a balance of research and teaching.


{{< /spoiler >}}


{{< spoiler text="__🖥️ If you are currently involved in research software development, please tell us a little bit about it?__" >}}

#### What languages do you use? 

Mostly Python for research software

#### What is the name and purpose of the software?

I'm working on LwimiLinks for my contract at SADiLaR. Currently I haven't started my Masters yet, so I'm not 100% sure what the software would be.

#### Do you do most of the development alone or do you work as part of a team? Tell us more? 

I think about 50-50. At SADiLaR I work in a team, whereas for my studies it is individual. 

#### Do you develop the software mainly for your own use or do others use it too? 

I think it's a mix of both. It's motivating to write something that I'll use myself, but I also like to see others get value out of it. 

{{< /spoiler >}}



{{< spoiler text="__:bulb: Where do you get training and support?__" >}}

#### Which communities of practice are you part of?

I think only this one, RSSE Africa. 

#### What training has had an impact on your current career?

I think that my undergrad and honours degrees in computer science really deepened my ability to comprehend and implement complex algorithms. Before, I knew how to program, but I feel better equipped to deal with novel research tasks than before. 

{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Do you see yourself as an academic, researcher, software engineer, technician…? All of it? Something else? A mix of one or two terms?__ " >}}

I see myself as a software engineer and researcher. 


{{< /spoiler >}}

{{< spoiler text="__:no_entry: What kind of barriers do you face in your work?__ " >}}

Access to source code and reproducibility of other papers can be an issue when running baselines for comparison. I also have some issues with quick access to GPU compute for iterating on different ideas (my research is in machine learning). 

{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: What part of your job do you enjoy the most?__" >}}

I enjoy solving problems and seeing the solution work as it should! This only happens some of the time, of course…

{{< /spoiler >}}

{{< spoiler text="__:sos: What would make things easier for you and support you in your work?__" >}}

Better reproducibility would be great (I believe RSSE Africa had a webinar on this recently)! 

{{< /spoiler >}}

{{< spoiler text="__:telescope: What are you looking forward to next year?__" >}}

I'm looking forward to being able to dedicate more time to research instead of working on assignments and studying for exams. 


{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Please share the most helpful career advice you’ve received that you want to share with other Africans in similar roles.__" >}}

The most helpful advice I've heard is to make the most of whatever opportunities you can get, and not to be afraid to apply for things even if you're not sure if you'll get in. 

{{< /spoiler >}}




