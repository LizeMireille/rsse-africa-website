---
title: "🇲🇬 October 2024: Meet Herman Randriamanantena"

subtitle: 

# Summary for listings and search engines
summary:  Welcome 👋 to our first October 2024 community member spotlight! In this month's edition we're introducing Herman Randriamanantena from Madagascar.

# Link this post with a project
projects: []

# Date published
date: '2024-10-29T10:00:00Z'


# Date updated
lastmod: '2024-10-29T10:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Herman Randriamanantena'
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Herman Randriamanantena

tags:
  - Community members
  - Astrophysics
  - Astronomy
  - Madagascar

categories:
  - Spotlights
  - Madagascar
---

Our first spotlight for October shines on Herman Randriamanantena from Madagascar. Herman is a Masters student in Astronomy and Astrophysics at the University of Antananarivo.

## In short
---

- __Preferred name and surname__: Herman Randriamanantena
- __Affiliation__ (where do you work or study): University of Antananarivo
- __Role__:   Msc Student in Astronomy & Astrophysics
- __Email__: herman.andriam@gmail.com or contact@h33.dev 
- __LinkedIn__: https://www.linkedin.com/in/h33rman 
- __Twitter(X)__: https://x.com/h33rman 
- __ORCID ID__: [0009-0004-3896-5534](https://orcid.org/0009-0004-3896-5534 )
- __Profile__: http://h33.dev 

---


{{< spoiler text="__:musical_score: What is your favourite song?__" >}}

:sound: [Etre Humain](https://youtu.be/gfVo39B92Ow?si=zh73sH6SH0KpCvs5) – _Nekfue_ 

{{< /spoiler >}}

{{< spoiler text="__:books: Can you describe your background and your current role?__" >}}

#### What did you study?

Astronomy & Astrophysics at the  University of Antananarivo, Madagascar

#### Do you have postgrad qualifications? In what area?

I have a Bachelor of Science Degree in Physics and Applications, University of Antananarivo and is currently enrolled for an Masters degree.

#### What is the title of your current role?

MSc student

#### Give a one-sentence summary of what you do in this role

My MSc project focuses on developing visualization tools to analyze and display the density distributions of galaxy properties using data simulations from the SIMBA cosmological model.

{{< /spoiler >}}

{{< spoiler text="__:briefcase: Describe a typical workday... Expand specifically on the amount of time you spend coding__" >}}

#### How much time do you spend coding?

I think, it’s around 3-5h per day

#### How many projects do you work on?

I'm currently focused on completing my MSc project. However, I also enjoy staying up-to-date with the latest courses and technologies in my field.

#### Give some keywords that summarise the topics of your projects.

Python, Data Visualization, Large Dataset

#### How often do you work with non-coding researchers?

I think most of the researchers I've worked with have had some coding experience.


{{< /spoiler >}}

{{< spoiler text="__:school: Tell us more about the organisation where you work?__" >}}

#### What is their primary objective?

SNOVIBOX is an IT company focused on sustainable digital solutions using custom applications and software deployment.

#### How many people in your organisation are involved in research software development (a thumbsuck is okay)?

I'd say around 5 of us focus on research software development.

{{< /spoiler >}}

{{< spoiler text="__:bulb: Where do you get training and support?__" >}}

#### Which communities of practice are you part of?

Malagasy Astronomical Society (MASS)

#### What training has had an impact on your current career?

The annual MASS Workshop has been particularly influential in my career. These workshops provide opportunities to learn about the latest research, network with other professionals, and develop new skills.

Curiosity has been a driving force behind my growth, and as a self-taught learner, I’m always seeking resources that deepen my skills. Among the most impactful have been a few key resources. First, I highly recommend the [Udemy](https://www.udemy.com/) course "[100 Days of Code: The Complete Python Pro Bootcamp](https://www.udemy.com/course/100-days-of-code/)" which has been instrumental in developing my Python skills.

I also benefited greatly from a learning opportunity provided by Orange Digital Center, which partnered with [Coursera](https://www.coursera.org/) to offer guided learning programs focused on job-relevant skills.

Lastly, discovering [Machine Learnia](https://www.youtube.com/@MachineLearnia) on YouTube was a turning point, as it is one of the best French-speaking channels for Data Science and Machine Learning, inspiring me to focus more deeply on data science

{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Do you see yourself as an academic, researcher, software engineer, technician…? All of it? Something else? A mix of one or two terms?__ " >}}

I primarily see myself as an academic researcher.


{{< /spoiler >}}

{{< spoiler text="__:no_entry: What kind of barriers do you face in your work?__ " >}}

The optimization of code

{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: What part of your job do you enjoy the most?__" >}}

Solving the real life problem with digitalization

{{< /spoiler >}}


{{< spoiler text="__:telescope: What are you looking forward to this year?__" >}}

I'm excited to complete my master's thesis this year. Afterward, I'm hoping to find a new opportunity in the field of data science.


{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Please share the most helpful career advice you’ve received that you want to share with other Africans in similar roles.__" >}}

The only way to learn is TO DO

{{< /spoiler >}}




