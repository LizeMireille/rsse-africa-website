---
title: "🇧🇼 April 2023: Meet Kushatha Ntwaetsile"

subtitle: 

# Summary for listings and search engines
summary:  Welcome 👋 to our fourth community  member spotlight! In this month's edition we're introducing Kushatha Ntwaetsile from Botswana.

# Link this post with a project
projects: []

# Date published
date: '2023-05-08T00:00:00Z'


# Date updated
lastmod: '2023-05-08T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Kushatha Ntwaetsile'
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Kushatha Ntwaetsile

tags:
  - Community members
  - Astrophysics
  - Botswana

categories:
  - Botswana
  - Spotlights
---

Our fourth spotlight shines on Kushatha Ntwaetsile from Botswana. Kushata is a Software and Data Processing Engineer at the Botswana International University of Science and Technology - SKA/AVN Department. 

## In short
---

- __Preferred name and surname__: Kushatha Ntwaetsile
- __Affiliation__ (where do you work or study): Botswana International University of Science and Technology - SKA/AVN Department
- __Role__: Software and Data Processing Engineer
- __Email__: kushkelly@gmail.com or ntwaetsilek@biust.ac.bw
- __ORCID__: [0000-0002-2424-2130](https://orcid.org/0000-0002-2424-2130)

---


{{< spoiler text="__:musical_score: What is your favourite song?__" >}}

:sound: I don’t have a favourite song, It just depends on my mood. Weird right? Currently I would say [Good Morning](https://www.youtube.com/watch?v=6CHs4x2uqcQ) by _Kanye West_ as I am looking forward to my graduation this year. 🎉🎉🎉 

{{< /spoiler >}}

{{< spoiler text="__:books: Can you describe your background and your current role?__" >}}

- _What did you study?_
- _Do you have postgrad qualifications? In what area?_

I graduated with an MSc Computer Science from BIUST in 2018. Before then, I had obtained my undergraduate Degree in Computer Systems Engineering from University of Sunderland through the Botswana Accountancy College in 2013. Recently, I successfully defended my thesis at the University of Hertfordshire-UK, obtaining a PhD in Astrophysics.

- _What is the title of your current role?_

I am a Software and Data Processing Engineer

- _Give a one-sentence summary of what you do in this role_

My role will include developing algorithms including machine learning algorithms that will be used to process large data sets from both the SKA and the AVN and also developing telescope operational software.

{{< /spoiler >}}

{{< spoiler text="__:briefcase: Tell us a bit more about your PhD project and the skills you learned through it?__" >}}

Radio astronomy is currently experiencing an unprecedented growth in the amount of data from large astronomical surveys and new techniques will be required to analyse these large data sets. Over the past decade machine learning in astronomy has evolved rapidly to match this increasing volume of data.
In my thesis, I implemented unsupervised machine learning models to automatically classify galaxies according to their morphology using data from the LOFAR Two-metre Sky Survey (LoTSS).

__Skills learned:__ coding in Python - I learned coding in Python when I started my PhD.
As I was from a Computer Science background, I learnt how to analyse and interpret Astronomical data.

__Soft skills:__ I learnt how to manage my time, as well as managing projects, I started my PhD as a new mum, I had to jaggle between being a student, mother and wife.

{{< /spoiler >}}


{{< spoiler text="__:briefcase: Describe a typical workday... Expand specifically on the amount of time you spend coding__" >}}

- _How much time do you spend coding?_

While doing my PhD, I used to spend a couple of hours a day coding in Python. I would say around 5 hours. But for this role, I think I will be spending the same number of hours or more as this project will be the first of its kind in Botswana.

- _How many projects do you work on?_

Currently I would be working as a software engineer for both the SKA/AVN Botswana project and the BotSat1 Project, which aims to launch Botswana’s first satellite late this year. My role will include designing and implementing the software systems for controlling the satellite and developing algorithms for analyzing the data collected by the satellite.

- _Give some keywords that summarise the topics of your projects._

  - Designing and implementing software systems.
  - Developing algorithms for analyzing  data.
  - Ensuring that the software and data processing systems are reliable and efficient.
  - Developing a plan for testing and validating the software and data processing systems.

- _How often do you work with non-coding researchers?_

Most of the time, I work on two projects which include non-coders.


{{< /spoiler >}}

{{< spoiler text="__:school: Tell us more about the organisation where you work?__" >}}

- _What is their primary objective?_

SKA/AVN Botswana Project - To establish the first radio observatory in Botswana which will be part of both the African VLBI and the SKA projects.

I am also part of the BotSat1 Project which aims to launch Botswana’s first satellite late this year.

- _How many people in your organisation are involved in research software development (a thumbsuck is okay)?_

Currently just me but I think some of my technical team members will be involved somehow.

{{< /spoiler >}}

{{< spoiler text="__:bulb: Where do you get training and support?__" >}}

- _Which communities of practice are you part of?_

I am part of the African Astronomical society which is a Pan-African Professional Society of Astronomers and will soon be part of the Astronomical society of Botswana which seeks to disseminate and share knowledge in the Astronomy domain in an effort to excite youngsters and interest all into learning about and participating in astronomy in Botswana.  

- _What training has had an impact on your current career?_

Research and development workshops that the doctoral college at the University of Hertfordshire used to offer during my PhD. This included topics such as Research Data Management, Communicating Your Research, Quantitative Data Analysis and Thesis Writing just to mention a few. I believe these positioned me well as a researcher in my field.


{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Do you see yourself as an academic, researcher, software engineer, technician…? All of it? Something else? A mix of one or two terms?__ " >}}


I think a mix of three terms, I have been an academic for the past few years as I was a tutor for the university of London where I supported their MSc Data Science programs. As a PhD student, I would say I have been a researcher for the past 4 years and lastly now I am a data processing Engineer.

{{< /spoiler >}}


{{< spoiler text="__:no_entry: What kind of barriers do you face in your work?__ " >}}

Being the only female in our department and people thinking being a woman and having a PhD is such an accomplishment. I don't attribute success to gender or race, I have a PhD because I am capable as a person and simply because I worked hard to be where I am.


{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: What part of your job do you enjoy the most?__" >}}

Outreach, getting to tell people about the work we do and seeing the excitement on their faces when they realize even in Botswana we have people who have studied Astrophysics.

{{< /spoiler >}}

{{< spoiler text="__:sos: What would make things easier for you and support you in your work?__" >}}

Having funding especially for mobility. Research and innovation needs some financial support for researchers to be able to attend conferences and even to go and benchmark from other researchers and Engineers in other countries. 

{{< /spoiler >}}

{{< spoiler text="__:telescope: What are you looking forward to this year?__" >}}

My PhD graduation in September, I just can't wait..


{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Please share the most helpful career advice you’ve received that you want to share with other Africans in similar roles.__" >}}

When I joined BIUST, someone said to me, workplaces always have their own  politics and that's not what you are here for. Just pick your lane, do your job, get paid and go home. Do what you are here for and when you look back you will see the mark you have left.


{{< /spoiler >}}




