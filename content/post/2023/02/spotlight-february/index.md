---
title: "🇪🇬 February 2023: Meet Mohammed Ali"
subtitle: 

# Summary for listings and search engines
summary: Welcome 👋 to our second community member spotlight! In this month's edition we're introducing Mohammed Ali from Egypt.

# Link this post with a project
projects: []

# Date published
date: '2023-03-01T00:00:00Z'

# Date updated
lastmod: '2023-03-01T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Mohammed Ali'
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Mohammed Ali

tags:
  - Community members
  - Data Analyst
  - Bioinformatics
  - Cheminformatics
  - Egypt

categories:
  - Egypt
  - Spotlights
---

Our second spotlight shines on Mohammed Ali from Egypt. Mohammed is a data analyst with a background in computer science.

## In short
---

- __Preferred name and surname__: Mohammed Ali
- __Affiliation__ (where do you work or study): Interstellar for Consulting
- __Role__: Data Analyst
- __Email__: moh_fcis@yahoo.com
- __LinkedIn__: https://www.linkedin.com/in/mohammedali85/
- __Twitter__: https://twitter.com/moh_fcis
- __Github__: https://github.com/MohammedFCIS, https://github.com/agenius-mohammed-ali


---


{{< spoiler text="__:musical_score: What is your favourite song?__" >}}

:sound: [Here I Am](https://www.youtube.com/watch?v=fIgfO9gD5GY) – _Bryan Adams_ 

{{< /spoiler >}}

{{< spoiler text="__:books: Can you describe your background and your current role?__" >}}

#### What did you study?

I am a scientific computing graduate from the Faculty of Computer Science, [Ain Shams University](https://www.asu.edu.eg/), Egypt. I also completed a Data Analysis Masters degree from the [Data ScienceTech Insitute](https://www.datasciencetech.institute/applied-msc-in-data-analytics/) in France.

#### What is the title of your current role?

Data Analyst 

#### Give a one-sentence summary of what you do in this role

Building different analysis and visualization tools for different bio data using R, shiny, SQL and other tools

{{< /spoiler >}}

{{< spoiler text="__:briefcase: Describe a typical workday... Expand specifically on the amount of time you spend coding__" >}}

#### How much time do you spend coding?

10 hours per day

#### How many projects do you work on?

Around 10 per year

#### Give some keywords that summarise the topics of your projects.

Data Wrangling, Interactive Plots, Shiny dashboards, software design, open source packages

#### How often do you work with non-coding researchers?

Nearly 20 to 30% of my time

{{< /spoiler >}}

{{< spoiler text="__:school: Tell us more about the organisation where you work?__" >}}

#### What is their primary objective?

Developing analytical solutions using a combination of R, Shiny, no-sql and SQL along with advanced data exploration and visualization tools targeting bioinformatics and chemoinformatic domains

#### How many people in your organisation are involved in research software development (a thumbsuck is okay)?

4 people

{{< /spoiler >}}

{{< spoiler text="__:bulb: Where do you get training and support?__" >}}

#### Which communities of practice are you part of?

Currently I am a part of:
- Africa R Group
- Egypt R Group
- SWB organization (USA)
- rOpenSci
- Statistical Methods training (USA)


#### What training has had an impact on your current career?

R and shiny development in:
- SWB organization (USA)
- rOpenSci
- Statistical Methods training (USA)


{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Do you see yourself as an academic, researcher, software engineer, technician…? All of it? Something else? A mix of one or two terms?__ " >}}


I am a researcher software engineer with analytical skills


{{< /spoiler >}}


{{< spoiler text="__:no_entry: What kind of barriers do you face in your work?__ " >}}

It is hard to find people that are both interested and able to contribute in bioinformatics and pharma building tools hence it is hard to continue in the field without support or getting a proper fund to continue


{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: What part of your job do you enjoy the most?__" >}}

Resolving a known issue for many users (like building dbparser package to ready DrugBank database)

{{< /spoiler >}}

{{< spoiler text="__:sos: What would make things easier for you and support you in your work?__" >}}

Finding support from bioinformatics, pharma or chemoinformatics people to give proper directions on the built tools (their feedback is always the golden compass for me)

{{< /spoiler >}}

{{< spoiler text="__:telescope: What are you looking forward to this year?__" >}}

- Releasing dbparser v2.0
- Relasing periscope2 v1.0
- Releasing DrugBrowser v1.0

{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Please share the most helpful career advice you’ve received that you want to share with other Africans in similar roles.__" >}}

Always participate in community projects, contribute to open source projects and help others that will help you and be a great investment in your career.


{{< /spoiler >}}




