---
title: "🇸🇳 December 2023: Meet Karim Mané"

subtitle: 

# Summary for listings and search engines
summary:  Welcome 👋 to our seventh community  member spotlight! In this month's edition we're introducing Karim Mané from Senegal.

# Link this post with a project
projects: []

# Date published
date: '2023-11-27T00:00:00Z'


# Date updated
lastmod: '2023-11-27T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Karim Mané'
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Karim Mané

tags:
  - Community members
  - London School of Hygiene and Tropical Medicine
  - Bioinformatics
  - Senegal

categories:
  - Spotlights
  - Senegal
---

Our December spotlight shines on Karim Mané from Senegal. Karim is a Research Software Engineer (RSE) for the  London School of Hygiene and Tropical Medicine (LSHTM) based at the Medical Research Council in The Gambia.

## In short
---

- __Preferred name and surname__: Karim Mané
- __Affiliation__ (where do you work or study): London School of Hygiene and Tropical Medicine
- __Role__:  Research Software Engineer
- __Email__: karimanee@outlook.com
- __LinkedIn__: https://www.linkedin.com/in/karim-man%C3%A9-7b928769/ 
- __ORCID ID__: 0000-0002-9892-2999
- __GitHub__:  https://github.com/Karim-Mane 

---


{{< spoiler text="__:musical_score: What is your favourite song?__" >}}

:sound: [MON ROI](https://www.youtube.com/watch?v=4BZw4r-7zfo) – _Youssoupha_ 

{{< /spoiler >}}

{{< spoiler text="__:books: Can you describe your background and your current role?__" >}}

#### What did you study?

I got my BSc in Bioinformatics-Biomathematics at Université Cheikh Anta Diop de Dakar in Senegal. I got my MSc in Bioinformatics at the same university.

#### What is the title of your current role?

I currently work as a Research software engineer (RSE) at the London School of Hygiene and Tropical Medicine (LSHTM) as part of the Epiverse-TRACE project.


#### Give a one-sentence summary of what you do in this role

I develop R packages for epidemiological data analysis.  

{{< /spoiler >}}

{{< spoiler text="__:briefcase: Describe a typical workday... Expand specifically on the amount of time you spend coding__" >}}

#### How much time do you spend coding?

I spend more than 90% of my daily time coding. 

#### How many projects do you work on?

I am involved in at least 3 projects.

#### Give some keywords that summarise the topics of your projects.

R, epidemiology, software development, early outbreak response

#### How often do you work with non-coding researchers?

In my current role, I mostly work with coding researchers. However, we collaborate with non-coding researchers and public health staff to account for their needs while developing the packages.

{{< /spoiler >}}

{{< spoiler text="__:school: Tell us more about the organisation where you work?__" >}}

#### What is their primary objective?

The LSHTM aims at improving health and health equity in the UK and worldwide; working in partnership to achieve excellence in public and global health research, education and translation of knowledge into policy and practice.


#### How many people in your organisation are involved in research software development (a thumbsuck is okay)?

The LSHTM have 3,300 staff based all around the world with core hubs in London and at the MRC Units in The Gambia and Uganda, which joined LSHTM in February 2018.

{{< /spoiler >}}

{{< spoiler text="__:bulb: Where do you get training and support?__" >}}

#### What training has had an impact on your current career?

A datacamp data science course I took at the beginning of my masters studies
I have learnt by myself and from the Senior RSE in my team about the best practices for R package development. This also includes some training to better understand git for a better collaborative package development.
{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Do you see yourself as an academic, researcher, software engineer, technician…? All of it? Something else? A mix of one or two terms?__ " >}}

I see myself as all of them. In addition to my role as an RSE, I participate in research activities by performing Statistical and Bioinformatics analysis for research groups based at the MRCG and the LSHTM. I provide support to Masters and PhD students. 

{{< /spoiler >}}

{{< spoiler text="__:no_entry: What kind of barriers do you face in your work?__ " >}}

I have the technical knowledge required for my position. We are building R packages for Epidemiologists and the wider scientific community. This sometimes requires a good understanding of epidemiology and disease modeling knowledge which I lack.

{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: What part of your job do you enjoy the most?__" >}}

Some parts of my work require more self investment than others. But I enjoy all of them, from the package conceptualisation, development, testing, to getting feedback from the end users, all of them.

{{< /spoiler >}}

{{< spoiler text="__:sos: What would make things easier for you and support you in your work?__" >}}

A good knowledge of Epidemiology and modeling concepts will make it easy to develop some packages and interact with the experts in those fields. Also some advanced R coding style will improve the structure of the packages we develop.

{{< /spoiler >}}

{{< spoiler text="__:telescope: What are you looking forward to this year?__" >}}

My goal for this year is to submit {readepi} to CRAN. This is an R package for importing data from health information systems.

{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Please share the most helpful career advice you’ve received that you want to share with other Africans in similar roles.__" >}}

Package development, and research in general, is a very tedious process. It requires dedication and passion. So do what you are passionate about and be perseverant.  

{{< /spoiler >}}




