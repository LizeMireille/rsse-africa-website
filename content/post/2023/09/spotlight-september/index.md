---
title: "🇿🇦 September 2023: Meet Eugene de Beste"

subtitle: 

# Summary for listings and search engines
summary:  Welcome 👋 to our fifth community  member spotlight! In this month's edition we're introducing Eugene de Beste from South Africa.

# Link this post with a project
projects: []

# Date published
date: '2023-09-28T00:00:00Z'


# Date updated
lastmod: '2023-09-28T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Eugene de Beste'
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Eugene de Beste

tags:
  - Community members
  - CSIR
  - CHPC
  - Bioinformatics
  - South Africa

categories:
  - South Africa
  - Spotlights
---

Our September spotlight shines on Eugene de Beste from South Africa. Eugene is a Senior High Performance Computing Technologist at the Centre for High Performance Computing (CHPC) of the Council for Scientific and Industrial Research (CSIR).

## In short
---

- __Preferred name and surname__: Eugene de Beste
- __Affiliation__ (where do you work or study): Centre for High Performance Computing at the Council for Scientific and Industrial Research
- __Role__: Senior High Performance Computing Technologist
- __Email__: eugenedb@pm.me
- __LinkedIn__: https://www.linkedin.com/in/eugene-de-beste/
- __Twitter__: https://twitter.com/edebeste

---


{{< spoiler text="__:musical_score: What is your favourite song?__" >}}

That’s a really tough one! I’d probably have to go with something from System of a Down. I am crazy about their energetic music, but I wouldn’t be able to nail down a specific track!

:sound: [System of a Down](https://www.youtube.com/@systemofadown)  

{{< /spoiler >}}

{{< spoiler text="__:books: Can you describe your background and your current role?__" >}}

#### What did you study?

For my bachelors I studied Computer Science at the University of the Western Cape, followed by an honors degree in Information Technology at the University of Cape Town. Finally, I did my masters in bioinformatics at the University of the Western cape once again.

#### What is the title of your current role?

Technically “Senior HPC Technologist”, but in reality more like “Senior Cloud and Platform Engineer”.

#### Give a one-sentence summary of what you do in this role

I work on automating and optimizing delivery of infrastructure in the complete stack, i.e. from the hardware to the cloud services.

{{< /spoiler >}}

{{< spoiler text="__:briefcase: Describe a typical workday... Expand specifically on the amount of time you spend coding__" >}}

#### How much time do you spend coding?

I focus on infrastructure, so I don’t work on application code too often, but I work on Infrastructure-as-Code on an almost daily basis. Terraform and Ansbile are my lifeblood.

#### How many projects do you work on?

I’m responsible for various projects at my organisation, I’d say two application software projects and numerous Infrastructure-as-Code projects.

#### Give some keywords that summarise the topics of your projects.

Cloud, Infrastructure, Helpdesk, GitOps, DevOps.

#### How often do you work with non-coding researchers?

Not that often in my current role.

{{< /spoiler >}}

{{< spoiler text="__:school: Tell us more about the organisation where you work?__" >}}

#### What is their primary objective?

I work for the Council for Scientific and Industrial Research (CSIR) of South Africa. Specifically, I work in their unit called the Centre for High Performance Computing (CHPC). The goal of the business unit is to enable research science through support and computing power. We offer the largest HPC cluster in South Africa and many research organisations and universities around the continent use our computing services.

#### How many people in your organisation are involved in research software development (a thumbsuck is okay)?

Maybe one or two at this stage.

{{< /spoiler >}}

{{< spoiler text="__:bulb: Where do you get training and support?__" >}}

#### Which communities of practice are you part of?

I was active with the RSSE Africa community, and helped in the creation of the RSSE-Africa forum, but I’ve sadly fallen out of activity in recent times. I’m working on getting back into it! Outside of that I’ve also been involved with cybersecurity communities.

#### What training has had an impact on your current career?

At this point, none really. Most of what I do is self taught!

{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Do you see yourself as an academic, researcher, software engineer, technician…? All of it? Something else? A mix of one or two terms?__ " >}}

I definitely fall more into the software engineer/systems administrator/cloud engineer space. I probably *(currently) mostly align with the title “platform engineer”, since I work quite a bit with backend platforms (including under-cloud).

{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: What part of your job do you enjoy the most?__" >}}

Implementing tools and technologies that make my and other people’s lives easier. Automation, mostly. I like to optimize processes that take up people’s time unnecessarily.

{{< /spoiler >}}

{{< spoiler text="__:sos: What would make things easier for you and support you in your work?__" >}}

I could always do with better structure and scope for work that needs to be done. We’re currently working on that and I’m already seeing improvements.

{{< /spoiler >}}

{{< spoiler text="__:telescope: What are you looking forward to this year?__" >}}

I have some up-coming conferences that I’m looking forward to. I’ll be attending the ZA NREN Conference in Cape Town this September. I’m also attending Kubecon and Supercomputing Conference 23 in the States later in November!


{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Please share the most helpful career advice you’ve received that you want to share with other Africans in similar roles.__" >}}

You should always hold the appetite for learning. People and events may sour your experience of your job or your motivation to work, but if you are excited about learning new things then you can fight the demotivation that often comes with interpersonal complications. Your work will always help someone!

{{< /spoiler >}}




