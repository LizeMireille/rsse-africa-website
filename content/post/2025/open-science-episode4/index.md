---
title: "Enabling Open Science through Research Code: Insights from Episode 4 - Documentation for Research Code "

subtitle: 

# Summary for listings and search engines
summary: RSSE Africa partnered with the RSE Asia Association, Talarify, ReSA and AREN to host a 6-month virtual community conversation series focusing on Enabling Open Science through Research Code. Episode 4 focused on code documentation. Read more in our blog post.


# Link this post with a project
projects: []

# Date published
date: '2025-02-19T00:00:00Z'

# Date updated
lastmod: '2025-02-19T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: ''
  focal_point: ''
  placement: 
  preview_only: true



authors:
  - Anelda van der Walt
  - Jyoti Bhogal
  - Jenny Wong
  - Joel Nitta
  - Mthetho Sovara

tags:
  - Open Science


categories:
  - Open Science
---

Imagine you’ve spent months crafting a complex research software tool, refining algorithms, and running countless simulations. It works brilliantly—but then, six months later, you revisit the code and… you’re lost. Sounds familiar? You’re not alone. Experts shared their experiences and strategies for writing good documentation in the fourth episode of the Research Software Community Conversations Series titled [Enabling Open Science through Research Code](../../../community_meetups/). Here’s what we learned from our participants and insightful panellists:

- [Jenny Wong](https://www.linkedin.com/in/thisisjennywong/) - Product Manager @ 2i2c (UK/US)
- [Joel Nitta](https://www.joelnitta.com/) - Associate Professor @ Chiba University (Japan)
- [Mthetho Sovara](https://www.linkedin.com/in/mthetho-sovara-3a939272/) - Centre for High-Performance Computing (CHPC), National Integrated Cyberinfrastructure System (NICIS) CSIR (South Africa)

### 📌 Why Documentation is Essential

🚀 __Boosts Reproducibility__: Research software often accompanies papers, but results can be difficult to replicate without documentation. As Jenny pointed out, in fluid dynamics research, well-documented simulation codes gained more adoption than poorly documented alternatives.

🤝 __Facilitates Collaboration__: Well-documented projects attract more contributors and streamline knowledge transfer. Joel shared his experience in the rOpenSci community, where clear documentation allowed him to take over and improve an existing R package without needing direct input from the original creator.

⏳ __Enhances Longevity__: Code without documentation quickly becomes unusable—even by its original authors! Mthetho described how well-documented software in weather forecasting led to unexpected collaborations with the South African Weather Service and Ethiopian researchers.

🔓 __Promotes Open Science__: Accessible research software fosters innovation and inclusion worldwide. Jenny emphasised that accessible documentation enables wider adoption of tools, making scientific computing more democratic.

### 🔥 Common Challenges & Solutions

⏰ __Time Constraints__ → Write minimal but clear documentation as you code. Joel suggested that legible code, such as meaningful variable names instead of “x” or “dat”, is itself a form of documentation that is even better than comments.

🧩 __Lack of Standardisation__ → Use templates and structured frameworks. Mthetho mentioned that clear, structured guides helped non-computational scientists successfully use OpenFOAM in unexpected fields like environmental science.

🎯 __Technical vs. User Documentation__ → Engage users in documentation efforts for clarity. Jenny highlighted the role of community-driven documentation efforts, such as hackathons, to improve usability.



### 🛠 Best Practices for Better Documentation


__✍️ 1. Write as You Code__

Waiting until the end leads to missing details. Add inline comments and README files while developing. Joel emphasised that tools like RMarkdown (and its successor, Quarto) can be used for literate programming—thereby ensuring that the results of code are automatically generated alongside their description.  

__🔄 2. Differentiate Documentation Types__

✔ User-facing documentation: How to install, use, and troubleshoot.

✔ Developer-facing documentation: Comments, API references, contribution guides.

__📚 3. Use the Right Tools__

📄 Markdown & Jupyter Notebooks – Combine narrative and code. 

📝 Quarto & R Markdown – For structured research outputs. 

📘 Sphinx & MIST – Python-based documentation frameworks. 

🌐 GitHub Wikis & Pages – Host documentation openly. 

🤖 AI-based tools (e.g., Cursor, Copilot) – Assist with documentation (but always review!).

__🤝 4. Community & Collaboration Matter__

💡 Participate in documentation hackathons. Jenny shared how Project Pythia’s hackathons provided a structured way to improve documentation in geospatial science. 

🌍 Join communities like Project Pythia, The Carpentries, and Write the Docs.

__💰 5. Advocate for Funding & Policy Support__

📢 Push for documentation funding in grant proposals.

🏛 Engage with funders to integrate documentation into open science policies. 


### 🚀 Future of Documentation: AI & Automation

🤖 Automated Documentation: AI tools generate initial drafts, saving researchers time. However, while helpful, AI-assisted documentation should always be reviewed for accuracy.

🌍 AI for Translation: Helps make documentation accessible in multiple languages, fostering global inclusivity. Anelda highlighted how AI tools might support research documentation in multilingual communities in the future. For now, we still need a human-in-the-loop translator for translation efforts. Especially for low-resourced languages.

⚠️ Caution Needed: AI-generated content must be reviewed for accuracy and ethical considerations. Jenny raised concerns about biases in AI and the environmental impact of large-scale machine learning models.


### 🎯 Final Thoughts: Make Documentation a Priority

Great research code deserves great documentation. It makes work reproducible, fosters collaboration, and ensures that software remains usable long after its creator has moved on. By embracing best practices and community support, we can all contribute to a culture where documentation is seen not as a burden—but as a fundamental part of open science.

__📢 Explore More:__

Access our [Resource Sheet](https://doi.org/10.5281/zenodo.14833913), which contains numerous valuable resources shared by the panellists, facilitators, and participants. 

View the [session recording](https://youtu.be/6GJZ8mO4t9Q) on YouTube.



_This meetup series is a collaboration between Talarify, RSSE Africa, RSE Asia, AREN, and ReSA._ 