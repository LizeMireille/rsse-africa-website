---
title: "🇿🇦 February 2025: Meet Pashlene Naidoo"

subtitle: 

# Summary for listings and search engines
summary:  Welcome 👋 to our first community member spotlight for 2025! In this month's edition we're introducing Pashlene Naidoo from South Africa.

# Link this post with a project
projects: []

# Date published
date: '2025-02-21T00:00:00Z'


# Date updated
lastmod: '2025-02-21T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Pashlene Naidoo'
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Pashlene Naidoo

tags:
  - Community members
  - Quantum Communication
  - South Africa

categories:
  - Spotlights
  - South Africa
---

Our February spotlight shines on Pashlene Naidoo from South Africa. Pashlene is a MSc Candidate in Quantum Communication at the Stellenbosch University in South Africa.

## In short
---

- __Preferred name and surname__: Pashlene Naidoo
- __Affiliation__ (where do you work or study): Stellenbosch University
- __Role__:  MSc Candidate in Quantum Communication
- __Email__: pashlenen@gmail.com
- __LinkedIn__:  https://www.linkedin.com/in/pashlene-naidoo-b142bb17a/
- __Twitter(X)__: https://x.com/pashlene
- __Instagram__: https://www.instagram.com/_pashlene_/?hl=en

---


{{< spoiler text="__:musical_score: What is your favourite song?__" >}}

:sound: [Eye of the Tiger](https://www.youtube.com/watch?v=btPJPFnesV4) – _Survivor_
{{< /spoiler >}}

{{< spoiler text="__:books: Can you describe your background and your current role?__" >}}

#### What did you study?

 I studied Applied Physics at the University of KwaZulu-Natal(UKZN) for both my undergraduate and honours degrees.


#### Do you have postgrad qualifications? In what area?

I completed my honours degree at the University of Kwa-Zulu Natal. My honors project was on Modelling the Quantum Noise of IBMQ Transmon Qubits using Hierarchical equations of motion (HEOM).

I’m currently pursuing my MSc in Physics at Stellenbosch University. My research is on encoding high-dimensional states for Quantum Communication.

#### What is the title of your current role?

MSc Candidate

#### Give a one-sentence summary of what you do in this role

I am a lab rat that works with lasers, studying the effects of spatial coherence on optical beams with the intent of producing optical fields that are less affected by the change’s atmospheric turbulence.

{{< /spoiler >}}

{{< spoiler text="__:briefcase: Describe a typical workday... Expand specifically on the amount of time you spend coding__" >}}

#### How much time do you spend coding?

About 6+ hours in the lab and 4+ hours coding(that’s if there’s bugs) most of the days.

#### How many projects do you work on?

My so-called 'projects' are all about giving back to the community. I serve as a teaching assistant at Stellenbosch University, where I assist first-year physics students. Additionally, I am involved in the Stellenbosch Student Laser Chapter, acting as both Treasurer and Social Media Manager. Our chapter organizes outreach projects, visiting high schools in the Western Cape to promote STEM education. I also participate in the STEM MentHer program, a dynamic year-long mentorship initiative that supports Grade 12 female students in STEM across major South African universities. Furthermore, I volunteer in the micro-school series hosted by NITheCS, delivering practical, peer-to-peer 20-minute talks focused on skill-sharing, emphasizing the transfer of practical 'how-to' skills.

#### Give some keywords that summarise the topics of your projects.

- Quantum
- Lasers
- Polarisers
- Python
- Filters
- Content Creator
- Machine Learning


#### How often do you work with non-coding researchers?

less often.

{{< /spoiler >}}

{{< spoiler text="__:school: Tell us more about the organisation where you work?__" >}}

#### What is their primary objective?

At Stellenbosch University the Physics Department is one of South Africa's leading research institutions, established in 1903 as one of the oldest Physics Departments in the country. The physics department is known for research excellence in areas like quantum technology, laser physics, astrophysics and nuclear physics, with a notable involvement in national and international scientific projects. 


#### How many people in your organisation are involved in research software development (a thumbsuck is okay)?

Too many to count.

{{< /spoiler >}}

{{< spoiler text="__🖥️ If you are currently involved in research software development, please tell us a little bit about it?__" >}}

#### What languages do you use? 

Python, along with libraries such as NumPy, OpenCV, TensorFlow/PyTorch, and Matplotlib for data processing, image analysis, and machine learning.

#### What is the name and purpose of the software?

The software does not have a specific name, but its purpose is to process vortex beam images, extract polarization and intensity information, and apply a convolutional neural network (CNN) to predict the topological charge of the beams.


#### Do you do most of the development alone or do you work as part of a team? Tell us more? 

I develop most of the software independently, tailoring it to my specific research needs. 

#### Do you develop the software mainly for your own use or do others use it too? 

The software is primarily developed for my research, but it could potentially be adapted for use by others working on similar problems in optics and machine learning applications in physics.


#### What else would you like to share about your software and the development experience? 

It is a rewarding experience. It has strengthened my programming skills and improved my ability to bridge theory and experiment. Additionally, working with real experimental data has helped me appreciate the challenges of noise, data preprocessing, and model optimization in machine learning applications.


{{< /spoiler >}}


{{< spoiler text="__:bulb: Where do you get training and support?__" >}}

#### Which communities of practice are you part of?

I am involved with the Stellenbosch Institute of Photonics and the Quantum Research Group, where we hold weekly meetings to share ideas and assist each other with research challenges.

The Stellenbosch Student Laser Chapter is part of the international network of SPIE (the International Society for Optics and Photonics) and Optica, offering professional development through workshops, talks, and networking events.

The National Institute for Theoretical and Computational Sciences (NITheCS) provides a broader platform for theoretical physics research, including quantum mechanics, through workshops, seminars, and funding for research projects. 

Additionally, the South African Quantum Technology Initiative (SAQuTI) allows us to engage with national efforts in quantum technology, providing opportunities for collaboration and funding.


#### What training has had an impact on your current career?

The guidance and advice from my supervisor has been invaluable.


{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Do you see yourself as an academic, researcher, software engineer, technician…? All of it? Something else? A mix of one or two terms?__ " >}}

I see myself as a Quantum Engineer/ Research Scientist/ Academic.

{{< /spoiler >}}

{{< spoiler text="__:no_entry: What kind of barriers do you face in your work?__ " >}}

There is a small number of postgraduate-students that are involved in Quantum Technology research. If the numbers increased, there would be an acceleration in the development of quantum technology in South Africa

{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: What part of your job do you enjoy the most?__" >}}

The freedom to pursue what I love and to be part of the next generation of technology—quantum technology. It’s a dream come true.

{{< /spoiler >}}

{{< spoiler text="__:sos: What would make things easier for you and support you in your work?__" >}}

Having more funding opportunities to attend conferences would be beneficial. It would provide an opportunity to network and collaborate with other physics departments both in South Africa and internationally, strengthening our local quantum technology community.

{{< /spoiler >}}

{{< spoiler text="__:telescope: What are you looking forward to this year?__" >}}

I look forward to finishing my masters project and thesis. Additionally, publishing a few papers and attending more International Conferences. 

{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Please share the most helpful career advice you’ve received that you want to share with other Africans in similar roles.__" >}}

Believe in yourself, no matter where you come from. With hard work and determination, prepare yourself to the best of your ability so you can seize opportunities as they arise.

{{< /spoiler >}}




