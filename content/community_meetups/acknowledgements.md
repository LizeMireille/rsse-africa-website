---
widget: blank
headless: true  # This file represents a page section.
title: "Acknowledgements" 
subtitle: 
weight: 60
design:
    columns: "1"
---

## Brainstorming and Inspiration from the African Reproducibility Network
---

In preparation for this series we spent several hours brainstorming with the co-founders of the African Reproducibility Network [Lamis Elkheir](https://www.linkedin.com/in/lamis-elkheir-b5844092/) and [Emmanuel Boakye](https://www.linkedin.com/in/emmaboakye/). Both Lamis and Emmanuel have extensive experience in creating accessible communities and we hoped to learn from their successes and challenges. We are tremendously greatful for their input, and even though we cannot implement all the ideas we came up with due to resource constraints, we hope to explore opportunities to co-organise more such events in the near future.
 

## Inspiration and Training Resources from CodeRefinery
---

The series agenda was inspired by content from [CodeRefinery](https://coderefinery.org/). CodeRefinery is a project within the [Nordic e-Infrastructure Collaboration](https://neic.no/). 
        
They offer training opportunities to researchers from Nordic research groups (but aim to expand beyond Nordics) to learn basic-to-advanced research computing skills and become confident in using state-of-the-art tools and practices from modern collaborative software engineering.

They also develop and maintain training material on software best practices for researchers that already write code. Their material addresses all academic disciplines and tries to be as programming language-independent as possible.

View their lessons, mostly developed under open licenses (e.g. CC-BY) [on their website](https://coderefinery.org/lessons/). 
