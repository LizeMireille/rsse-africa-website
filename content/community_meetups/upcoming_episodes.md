---
widget: blank
headless: true  # This file represents a page section.
title: "Upcoming Episodes" 
subtitle: |
    Register today to join the conversations!
weight: 20

design:
    columns: "1"
---


<div class="row">
    <h3> <a href="../events-rsse-africa/2025-03-20/">Episode 6: Research Software Funding</a></h3>   
</div>

20 March 2025 @ 08:30 - 10:00 UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Research+Software+Funding&iso=20250320T0830&p1=1440&ah=1&am=30">(find your local time here)</a>

<a href="https://us06web.zoom.us/meeting/register/tZwodO2pqD0rGtUTKdPjxP2j2X1gFioPn2bo" target="_blank"><img src="../static-images/register-button.png"></a>
