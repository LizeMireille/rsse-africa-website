---
# Display name
title: Saranjeet Kaur Bhogal

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "Research Software Engineer"

user_groups: ["Episode 5 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Imperial College London
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |


# Interests to show in About widget
interests: 
  - Saranjeet Kaur Bhogal is a Research Software Engineer at Imperial College London. In 2023, she was selected an International Fellow of the Software Sustainability Institute. Her academic background includes an MPhil in Statistics. Throughout her career, she has been involved with various software engineering communities. In 2021, she participated in the Open Life Science program, where she co-founded the Research Software Engineering (RSE) Asia Association. She has represented the RSE Asia community at events in Bhutan, Nepal, Sri Lanka, and the UK.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: 
  icon_pack: 
  link: 
- icon: 
  icon_pack: 
  link: 



# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---

