---
# Display name
title: Mr Mahmoud Abdelrazek

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "Senior Research Data Consultant"

user_groups: ["Episode 1 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Advanced Research Computing, University College London, UK
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |

# Interests to show in About widget
interests: 
 - <br> I'm a Data Consultant / Steward from Portsaid, Egypt, working at the Advanced Research Computing, University College London. I like reading 📚 running 🏃🏾‍♂️ swimming 🏊🏾‍♂️ cycling 🚴 and thinking about data problems. My background is in geology, databases, spatiotemporal and medical data analytics. 


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/razekmh/ 
# - icon: building-user
#   icon_pack: fa
#   link: https://www.ndcn.ox.ac.uk/team/gaurav-bhalerao
- icon: user
  icon_pack: fa
  link: https://www.razekmh.dev/
# - icon: envelope
#   icon_pack: fa
#   link: mailto:prasadsut11@gmail.com

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true



---

