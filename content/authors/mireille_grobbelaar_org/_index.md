---
# Display name
title: Mireille Grobbelaar

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "MSc Data Science Candidate"

user_groups: ["Programme Committee"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Independent
  - url: https://www.linkedin.com/in/mireille-grobbelaar/

# Short bio (displayed in user profile at end of posts)
bio: 
  

# Interests to show in About widget
interests:
  - Mireille qualified with an MSc in Physiological Sciences after which she enrolled for a MSc in Data Science. She's currently a parttime learning assistant for data visualisation and AI at 2U (edX) and have been instrumental to the operations of RSSE Africa since 2023.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/mireille-grobbelaar/
- icon: envelope
  icon_pack: fa
  link: "mailto:grobbelaar.mireille@gmail.com "



# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---

