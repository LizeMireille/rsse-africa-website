---
# Display name
title: Richard Dushime

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "Community Focused and Open Source Developer"

user_groups: ["Programme Committee"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Independent
  - url: https://www.linkedin.com/in/richard-dushime/

# Short bio (displayed in user profile at end of posts)
bio: 
  

# Interests to show in About widget
interests:
  - Richard is a passionate open source contributer and organiser of Google Developer Groups in Sub-Saharan Africa. He's been involved in RSE communities as secretary of the RSE Asia Australia Conference and independent contributor to RSSE Africa. Richard is committed to building and empowering communities in research software engineering and data science realms. 


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/richard-dushime/
- icon: envelope
  icon_pack: fa
  link: "mailto:mudaherarich@gmail.com "



# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---

