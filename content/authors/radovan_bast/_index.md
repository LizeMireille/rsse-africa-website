---
# Display name
title: Radovan Bast

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "<br> Research Software Engineer"

user_groups: ["Episode 2 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: UiT- University of Tromsø  / CodeRefinery
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |

# Interests to show in About widget
interests: 
 - <br> Radovan Bast is a research software engineer with a background in theoretical chemistry. He has previously worked in France and Sweden, and he is currently based in Norway. Radovan’s work is at the border between science, software, and computational support, and he enjoys supporting multi-disciplinary research. Radovan now works as part of the Norwegian Research Infrastructure Services at the University of Tromsø (UiT), Norway, and leads the CodeRefinery project. At UiT he leads the high-performance computing group and the research software engineering group. His goal is to make computing and programming more accessible and usable through training.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: user
  icon_pack: fa
  link: https://bast.fr/
- icon: envelope
  icon_pack: fa
  link: mailto:radovan.bast@uit.no

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true



---

