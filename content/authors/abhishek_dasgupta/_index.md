---
# Display name
title: Abhishek Dasgupta

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "Senior Research Software Engineer"

user_groups: ["Episode 5 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Oxford Research Software Engineering group, University of Oxford
  - url: https://abhidg.name

# Short bio (displayed in user profile at end of posts)
bio: 
  

# Interests to show in About widget
interests:
  - "I am a Research Software Engineer with the University of Oxford RSE group, interested in software development productivity, best practices, and reproducibility of research software through training and outreach. I am particularly interested in improving code testing, and improving productivity through the use of type checkers, linters and through deeper knowledge of the ecosystem of tools that are available to improve research software. Prior to my current role as a research software engineer, I did my DPhil in Computer Science, before moving to a postdoctoral position in the Department of Politics and International Relations at Oxford."


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: user
  icon_pack: fa
  link: https://orcid.org/0000-0003-4420-0656



# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---

