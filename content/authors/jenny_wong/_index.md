---
# Display name
title: Jenny Wong (she/her)
# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "<br> <br> <br> <br> Product Manager"

user_groups: ["Episode 4 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: 2i2c (US-based organisation)
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |

# Interests to show in About widget
interests: 
 - <br> Jenny is a Technical Content Developer at 2i2c, a US-based non-profit that provides a global network of interactive computing platforms for research and education communities. She primarily supports folks with getting the most out of cloud-native workflows through documentation, training and community engagement. She is passionate about democratising access to large-scale, scientific cyber-infrastructure and loves using her deep technical knowledge and friendly communication skills to bridge together technology and people.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: user
  icon_pack: fa
  link: https://www.linkedin.com/in/thisisjennywong/
- icon: github
  icon_pack: fab
  link: https://github.com/jnywong
- icon: hashtag
  icon_pack: fa
  link: https://bsky.app/profile/jnywong.bsky.social
- icon: envelope
  icon_pack: fa
  link: "mailto:jwong@2i2c.org"

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true



---

