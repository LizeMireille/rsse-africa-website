---
# Display name
title: Mthetho Vuyo Sovara
# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "<br> Research Support Scientist"

user_groups: ["Episode 4 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Centre for High Performance Computing (CHPC), National Integrated Cyberinfrastructure System (NICIS), South Africa
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |

# Interests to show in About widget
interests: 
 - <br> I am passionate about understanding ocean-atmosphere interactions in southern Africa. My research leverages advanced research software, computational models, and data science techniques powered by high-performance computing (HPC) to improve weather and climate forecasts, supporting climate policy and tackling environmental challenges. Additionally, I work to empower postgraduate students across southern Africa through training in coding, research software, and HPC optimization. My initiatives span from high school outreach programs to advanced university workshops, fostering technical expertise and innovation in climate and computational research. These efforts aim to build capacity and advance scientific understanding in the region.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: user
  icon_pack: fa
  link: https://www.linkedin.com/in/mthetho-sovara-3a939272/
- icon: envelope
  icon_pack: fa
  link: "mailto:msovara@csir.co.za"
- icon: envelope
  icon_pack: fa
  link: mailto:mvsovara@gmail.com

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true



---

