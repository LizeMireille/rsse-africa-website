---
# Display name
title: "Sheena O'Connell"

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "Founder"

user_groups: ["Episode 5 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Prelude 
  - url: https://www.linkedin.com/in/sheena-o-connell-0bb72527/  

# Short bio (displayed in user profile at end of posts)
bio: 
  

# Interests to show in About widget
interests:
  - I'm a software engineer and technical leader who has spent the last 5+ years reimagining how we teach coding. I’ve built alternative education systems, focusing on effective teaching practices and organizational structures. I founded the Guild of Educators to support tech educators and recently founded Prelude, offering technical training and consulting for educators and organizations. I'm active in the global Python and Django communities, organizing events like PyCon Africa and DjangoCon Africa. I'm a Django Software Foundation member and the co-chair of the Python Software Foundation's Education and Outreach workgroup.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/sheena-o-connell-0bb72527/ 
- icon: envelope
  icon_pack: fa
  link: "mailto:sheena@prelude.tech"
- icon: user
  icon_pack: fa
  link: https://fosstodon.org/@sheena



# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---

