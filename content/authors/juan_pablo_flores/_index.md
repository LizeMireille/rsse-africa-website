---
# Display name
title: Juan Pablo Flores

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "<br> Senior Program Manager"

user_groups: ["Episode 3 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: GitHub
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |

# Interests to show in About widget
interests: 
 - <br> Juan Pablo Flores is a Program Manager at GitHub, where he supports the creation of opportunities for people from diverse backgrounds to learn programming and fosters connections among different technical communities. He has worked on research in Human-Computer Interaction (HCI), focusing on how individuals collaborate on online platforms, and has explored the social dynamics of live streaming. His recent work includes advancing Open Science initiatives and measuring the impact of artificial intelligence in education, with the goal of leveraging technology for more inclusive and effective learning environments.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: user
  icon_pack: fa
  link: https://orcid.org/0009-0003-0208-8480
- icon: envelope
  icon_pack: fa
  link: mailto:juanpflores@github.com

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true



---

